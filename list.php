<?php
require_once "db_connect.php";
$db = new DB_CONNECT();
$sql = "select * from tbl_post";
$result = mysqli_query($db->connect(),$sql);

?>
<?php 
session_start();

	include("connection.php");
	include("functions.php");

	$user_data = check_login($con);

?>
<!DOCTYPE html>
<html lang="en">
     <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,700&display=swap" rel="stylesheet">
        <title>Blog Home</title>
        <link href="css/styles.css" rel="stylesheet" />
        <link href="css/menu.css" rel="stylesheet" />
        <link href="css/modern.css" rel="stylesheet"  />
        <script src="https://kit.fontawesome.com/332a215f17.js" crossorigin="anonymous"></script>
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
    </head>
    <body>
        <div>
        <!--Nav-->
 <nav class="navbar navbar-expand-sm navbar-dark bg-black">
            <div class="container">
            <a href="#" class="navbar-brand">BRAND.WATCH</a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a href="index.php"class="nav-link">Home</a>
                    </li>
                    <li class="nav-item" >                  
                        <a href="category.php" class="nav-link">Category</a>          
                    </li>
                    <li class="nav-item">
                        <a href="product.php" class="nav-link">Products</a>
                    </li>
                    <li class="nav-item">
                        <a href="list.php" class="nav-link">About</a>
                    </li>
                    <li class="nav-item">
                        <a href="contact.php" class="nav-link">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link"><i class="fas fa-shopping-cart fa-2x"></i></a>
                    </li>
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $user_data['user_name']; ?></span>
                            <img class="img-profile rounded-circle"
                                src="img/undraw_profile.svg" style="width:35px ;height:35px; text-center: left;">
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                            aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                Profile
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                Settings
                            </a>
                            <a href="logout.php">Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
            </div>
        </nav>
    <div class="container">
        <h2>Post List</h2>
        <a href="create.php" class="btn btn-success">Add New</a>
        <br /><br>
        <table class="table">
            <thead>
                <tr>
                    <th>UID</th>
                    <th>Title</th>
                    <th>Description</th>
                    <th>category</th>
                    <th>Picture</th>
                    <th>Delete</th>
                    <th>Edit</th>
                </tr>
            </thead>
            <tbody>
                <?php
                if (mysqli_num_rows($result) > 0){
                    while($row = mysqli_fetch_assoc($result)){
                        ?>
                        <tr>
                            <td><?php echo $row['uid']?></td>
                            <td><?php echo $row['title']?></td>
                            <td><?php echo $row['description']?></td>
                            <td>
                                <?php 
                                   $cate = mysqli_query($db->connect(),"SELECT * FROM tbl_category where uid=".$row['category']);
                                   if(mysqli_num_rows($cate)> 0) if($record = mysqli_fetch_array($cate)) {echo $record['name'];} 
                                ?>
                            </td>
                            <td><img width='200px' src='upload/<?php echo $row['picture']?>' alt='picture' /></td>
                            <td><button onclick="removePost('<?php echo $row['uid']?>','<?php echo $row['picture']?>')" href='delete.php?uid=<?php echo $row['uid']?>' class='btn btn-danger'>Delete</button></td>
                            <script>
                                function removePost(uid,oldPicture){
                                    if (confirm('Are you sure you want to delete this record?')) {
                                        window.location.href = "delete.php?uid="+uid+"&oldPicture="+oldPicture;
                                        //console.log('Record was deleted.');
                                    }
                                }
                            </script>
                            <td><a href='edit.php?uid=<?php echo $row['uid']?>' class='btn btn-info'>Edit</button></td>
                        </tr>
                        <?php
                    }
                }
                ?>

            </body>
        </table>
    </div>
</body>
</html>