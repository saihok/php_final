<?php
require_once "db_connect.php";
$db = new DB_CONNECT();
$sql = "select * from tbl_post where uid=".$_GET['uid'];
$result = mysqli_query($db->connect(),$sql);
if (mysqli_num_rows($result)> 0) {
    $row = mysqli_fetch_assoc($result);
}

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
        <meta name="description" content="" />
        <meta name="author" content="" />
        <title>Blog Post</title>
        <!-- Core theme CSS (includes Bootstrap)-->
        <link href="css/styles.css" rel="stylesheet" />
        <link href="css/menu.css" rel="stylesheet" />
        <link href="css/modern.css" rel="stylesheet" />
    </head>
    <body>
        <!-- Navigation-->
        <nav class="navbar navbar-expand-sm navbar-dark bg-black">
            <div class="container">
            <a href="#" class="navbar-brand">BRAND.WATCH</a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                <li class="nav-item active">
                    <a href="home.php"class="nav-link">Home</a>
                </li>
                <li class="nav-item">
                    <a href="category.php" class="nav-link">Category</a>
                </li>
                <li class="nav-item">
                    <a href="product.php" class="nav-link">Products</a>
                </li>
                <li class="nav-item">
                    <a href="list.php" class="nav-link">About</a>
                </li>
                <li class="nav-item">
                    <a href="contact.php" class="nav-link">Contact</a>
                </li>
                <li class="nav-item">
                    <a href="#" class="nav-link"><i class="fas fa-shopping-cart fa-2x"></i></a>
                </li>
                </ul>
            </div>
            </div>
        </nav>
        
        <!-- Page content-->
        <div class="container">
            <div class="row">
                <!-- Post content-->
                <div class="col-lg-8">
                    <!-- Title-->
                    <h1 class="mt-4"><?php echo $row['title']?></h1>
                    <!-- Author-->
                    <p class="lead">
                        by
                        <a href="#!">saihok</a>
                    </p>
                    <hr />
                    <!-- Date and time-->
                    <p>Posted on January 1, 2021 at 12:00 PM</p>
                    <hr />
                    <!-- Preview image-->
                    <img class="img-fluid rounded" src="upload/<?php echo $row['picture']?>" width="900px" alt="..." />
                    <hr />
                    <!-- Post content-->
                    <p class="lead"><?php echo $row['description']?></p><hr />
                    <!-- Comments form-->
                    
                </div>
                <!-- Sidebar widgets column-->
                <div class="col-md-4">
                    <!-- Search widget-->
                    <div class="card my-4">
                        <h5 class="card-header">Search</h5>
                        <div class="card-body">
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Search for..." />
                                <span class="input-group-append"><button class="btn btn-secondary" type="button">Go!</button></span>
                            </div>
                        </div>
                    </div>
                    <!-- Categories widget-->
                    <div class="card my-4">
                        <h5 class="card-header">Categories</h5>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <ul class="list-unstyled mb-0">
                                        <li><a href="#!">Web Design</a></li>
                                        <li><a href="#!">HTML</a></li>
                                        <li><a href="#!">Freebies</a></li>
                                    </ul>
                                </div>
                                <div class="col-lg-6">
                                    <ul class="list-unstyled mb-0">
                                        <li><a href="#!">JavaScript</a></li>
                                        <li><a href="#!">CSS</a></li>
                                        <li><a href="#!">Tutorials</a></li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- Side widget-->
                    <div class="card my-4">
                        <h5 class="card-header">Side Widget</h5>
                        <div class="card-body">You can put anything you want inside of these side widgets. They are easy to use, and feature the new Bootstrap 4 card containers!</div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Footer-->
        <footer class="py-5 bg-dark">
            <div class="container"><p class="m-0 text-center text-white">Copyright &copy; Your Website 2021</p></div>
        </footer>
        <!-- Bootstrap core JS-->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
    </body>
</html>
