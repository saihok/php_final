<?php
require_once "db_connect.php";
$db = new DB_CONNECT();
if(isset($_GET['uid'])){
    $sql = "select * from tbl_post where category=".$_GET['uid'];
} else {
    $sql = "select * from tbl_post";
}
$result = mysqli_query($db->connect(),$sql);

?>
<?php 
session_start();

	include("connection.php");
	include("functions.php");

	$user_data = check_login($con);

?>

<!DOCTYPE html>
<html lang="en">
     <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,700&display=swap" rel="stylesheet">
        <title>Blog Home</title>
        <link href="css/styles.css" rel="stylesheet" />
        <link href="css/menu.css" rel="stylesheet" />
        <link href="css/modern.css" rel="stylesheet"  />
        <script src="https://kit.fontawesome.com/332a215f17.js" crossorigin="anonymous"></script>
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
    </head>
    <body>
        <div>
        <!--Nav-->
 <nav class="navbar navbar-expand-sm navbar-dark bg-black">
            <div class="container">
            <a href="#" class="navbar-brand">BRAND.WATCH</a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a href="index.php"class="nav-link">Home</a>
                    </li>
                    <li class="nav-item" >                  
                        <a href="category.php" class="nav-link">Category</a>          
                    </li>
                    <li class="nav-item">
                        <a href="product.php" class="nav-link">Products</a>
                    </li>
                    <li class="nav-item">
                        <a href="list.php" class="nav-link">About</a>
                    </li>
                    <li class="nav-item">
                        <a href="contact.php" class="nav-link">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link"><i class="fas fa-shopping-cart fa-2x"></i></a>
                    </li>
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $user_data['user_name']; ?></span>
                            <img class="img-profile rounded-circle"
                                src="img/undraw_profile.svg" style="width:35px ;height:35px; text-center: left;">
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                            aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                Profile
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                Settings
                            </a>
                            <a href="logout.php">Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
            </div>
        </nav>
        
      
        <br>     
        <div class="container">
            <div class="row">
                <!-- Post content-->
                <div class="col-lg-8">
                    <h1 class="my-4">
                            Page Heading
                            <small>Secondary Text</small>
                        </h1>
                        <!-- Blog post-->
                        <?php
                        if (mysqli_num_rows($result) > 0){
                            while($row = mysqli_fetch_assoc($result)){
                            ?>
                                <div class="container">
                                    <div class="row">
                                        <div class="col-md-10">
                                            <div class="card mb-2">
                                                <img class="card-img-top" src="upload/<?php echo $row['picture']?>" width="100%" alt="Card image cap" />
                                                <div class="card-body">
                                                    <h2 class="card-title"><?php echo $row['title']?></h2>
                                                    <p class="card-text"><?php echo $row['description']?></p>
                                                    <a class="btn btn-success" href="detail.php?uid=<?php echo $row['uid']?>">Read More →</a>
                                                </div>
                                                <!-- <div class="card-footer text-muted">
                                                    Posted on January 1, 2021 by
                                                    <a href="#!">Start Bootstrap</a>
                                                </div> -->
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            <?php
                            }
                        }       
                        ?>                  
                </div>
                <!-- Sidebar widgets column-->
                <div class="col-md-4" style="background-color:dark ;">
                    <!-- Search widget-->
                    <div class="card my-4">
                        <h5 class="card-header">Search</h5>
                        <div class="card-body">
                            <div class="input-group">
                                <input class="form-control" type="text" placeholder="Search for..." />
                                <span class="input-group-append"><button class="btn btn-secondary" type="button">Go!</button></span>
                            </div>
                        </div>
                    </div>
                    <!-- Categories widget-->
                    <div class="card my-4">
                        <h5 class="card-header">Categories</h5>
                        <div class="card-body">
                            <div class="row">
                                <div class="col-lg-6">
                                    <ul class="list-unstyled mb-0" >
                                        <?php
                                            $con = mysqli_connect('localhost', 'root', '', 'up_php');
                                            $result = mysqli_query($con, "SELECT * FROM tbl_category");
                                            while ($row = mysqli_fetch_array($result)) {
                                            ?>
                                                <li ><a href="category.php?uid=<?php echo $row['uid']?>" class="nav-link" style="color: grey;"><?php echo $row['name']?></a></li>
                                            <?php
                                            }
                                        ?>
                                    </ul>
                                </div>
                            
                                <!-- <div class="col-lg-6">
                    
                                </div> -->
                            </div>
                        </div>
                    </div>
                    <!-- Side widget-->
                    <div class="card my-4">
                        <h5 class="card-header">Creat Record</h5>
                        <a href="create.php" class="btn btn-success">Add New</a>
                    </div>

                    <div class="card my-4">
                        <h5 class="card-header">Post List</h5>
                        <a href="list.php" class="btn btn-success">Add New</a>
                    </div>
                </div>
            </div>
        </div>

        <br>
        <br>                                   
        <hr>


        <section class="contact py-5">
            <div class="container">
                <h2 class="section-heading">Contact Us</h2>
                <form class="col-lg-6 offset-lg-3">
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" id="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" class="form-control" placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea class="form-control" id="message" placeholder="Type your message" row="5">
                        
                    </textarea>
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="check">
                        <label class="form-check-label" for="check">Subscribe to newsletter</label>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-lg btn-color cont-btn">Submit</button>
                    </div>
                </form>
            </div>
        </section>
        <!--End of Contact Section-->
        <!--Footer-->
        <footer class="footer mt-5">
            <div class="text-center py-5">
                <h2 class="py-3">Time Value</h2>
                <div class="mx-auto heading-line"></div>
            </div>
            <div class="container">
                <div class="row mb-3">
                    <div class="col-lg-8 offset-lg-2 text-center">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean fringilla aliquet est nec aliquet. 
                            Cras convallis ultrices sem, id cursus tellus varius. </p>
                        <div class="justify-content-center">
                            <i class="fab fa-facebook fa-2x"></i>
                            <i class="fab fa-twitter fa-2x"></i>
                            <i class="fab fa-instagram fa-2x"></i>
                            
                            </div>
                    </div>
                </div>
                <div class="copyright text-center py-3 border-top text-light">
                    <p>&copy; Copy Right Time Value</p>
                    
                </div>
            </div>
        </footer>
        <!-- Optional JavaScript -->
        <!-- jQuery first, then Popper.js, then Bootstrap JS -->
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
    </body>
</html>      
