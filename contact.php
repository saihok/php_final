<?php
if($_SERVER['REQUEST_METHOD'] == "POST") {
    $name = isset($_POST['name'])?$_POST['name']:"";
    $smg = isset($_POST['message'])?$_POST['message']:"";
    $email = isset($_POST['email'])?$_POST['email']:"";
    $to      = 'admin@example.com';
    $subject = 'the subject';
    $message = $smg. " from " . $name . "email : ".$email;
    // Sending email
	$headers = "From:saihok71@gmail.com\r\n";
	$headers .= "Reply-To: saihok71@gmail.com\r\n";
	$headers .= "CC: up@example.com\r\n";
	$headers .= "MIME-Version: 1.0\r\n";
	$headers .= "Content-Type: text/html; charset=ISO-8859-1\r\n";
	
    if(mail($to, $subject, $message, $headers)){
        $smg = 'mail sent successfully.';
    } else{
        $smg = 'Unable to send email. Please try again.';
    }
}
?>
<?php 
session_start();

	include("connection.php");
	include("functions.php");

	$user_data = check_login($con);

?>
<!DOCTYPE html>
<html lang="en">
     <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <meta name="description" content="" />
        <meta name="author" content="" />
        <!-- Bootstrap CSS -->
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css" integrity="sha384-Vkoo8x4CGsO3+Hhxv8T/Q5PaXtkKtu6ug5TOeNV6gBiFeWPGFN9MuhOf23Q9Ifjh" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Poppins:400,700&display=swap" rel="stylesheet">
        <title>Blog Home</title>
        <link href="css/styles.css" rel="stylesheet" />
        <link href="css/menu.css" rel="stylesheet" />
        <link href="css/modern.css" rel="stylesheet"  />
        <script src="https://kit.fontawesome.com/332a215f17.js" crossorigin="anonymous"></script>
        <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
        <link
            href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
            rel="stylesheet">

        <!-- Custom styles for this template-->
        <link href="css/sb-admin-2.min.css" rel="stylesheet">
    </head>
    <body>
        <div>
        <!--Nav-->
 <nav class="navbar navbar-expand-sm navbar-dark bg-black">
            <div class="container">
            <a href="#" class="navbar-brand">BRAND.WATCH</a>
            <button class="navbar-toggler" data-toggle="collapse" data-target="#navbarCollapse">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="navbarCollapse">
                <ul class="navbar-nav ml-auto">
                    <li class="nav-item active">
                        <a href="index.php"class="nav-link">Home</a>
                    </li>
                    <li class="nav-item" >                  
                        <a href="category.php" class="nav-link">Category</a>          
                    </li>
                    <li class="nav-item">
                        <a href="product.php" class="nav-link">Products</a>
                    </li>
                    <li class="nav-item">
                        <a href="list.php" class="nav-link">About</a>
                    </li>
                    <li class="nav-item">
                        <a href="contact.php" class="nav-link">Contact</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link"><i class="fas fa-shopping-cart fa-2x"></i></a>
                    </li>
                    <li class="nav-item dropdown no-arrow">
                        <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button"
                            data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                            <span class="mr-2 d-none d-lg-inline text-gray-600 small"><?php echo $user_data['user_name']; ?></span>
                            <img class="img-profile rounded-circle"
                                src="img/undraw_profile.svg" style="width:35px ;height:35px; text-center: left;">
                        </a>
                        <!-- Dropdown - User Information -->
                        <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in"
                            aria-labelledby="userDropdown">
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                                Profile
                            </a>
                            <a class="dropdown-item" href="#">
                                <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                                Settings
                            </a>
                            <a href="logout.php">Logout</a>
                        </div>
                    </li>
                </ul>
            </div>
            </div>
        </nav>
            
            <!-- Page content-->
            
        <section class="contact py-5">
            <div class="container">
                <h2 class="section-heading">Contact Us</h2>
                <h2><?php if(!empty($smg)) echo $smg;?></h2>
                <form class="col-lg-6 offset-lg-3" action="contact.php" method="post" >
                    <div class="form-group">
                        <label for="email">Email address</label>
                        <input type="email" id="email" class="form-control" aria-describedby="emailHelp" placeholder="Enter email">
                        </div>
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" id="name" class="form-control" placeholder="Enter name">
                    </div>
                    <div class="form-group">
                        <label for="message">Message</label>
                        <textarea class="form-control" id="message" placeholder="Type your message" row="5">
                        
                    </textarea>
                    </div>
                    <div class="form-group form-check">
                        <input type="checkbox" class="form-check-input" id="check">
                        <label class="form-check-label" for="check">Subscribe to newsletter</label>
                    </div>
                    <div class="text-center">
                        <button class="btn btn-lg btn-color cont-btn">Submit</button>
                    </div>
                </form>
            </div>
        </section>
                
        <footer class="footer mt-5">
            <div class="text-center py-5">
                <h2 class="py-3">Time Value</h2>
                <div class="mx-auto heading-line"></div>
            </div>
            <div class="container">
                <div class="row mb-3">
                    <div class="col-lg-8 offset-lg-2 text-center">
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Aenean fringilla aliquet est nec aliquet. 
                            Cras convallis ultrices sem, id cursus tellus varius. </p>
                        <div class="justify-content-center">
                            <i class="fab fa-facebook fa-2x"></i>
                            <i class="fab fa-twitter fa-2x"></i>
                            <i class="fab fa-instagram fa-2x"></i>
                            
                            </div>
                    </div>
                </div>
                <div class="copyright text-center py-3 border-top text-light">
                    <p>&copy; Copy Right Time Value</p>
                    
                </div>
            </div>
        </footer>

        <!-- Bootstrap core JS-->
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
        <!-- Core theme JS-->
        <script src="js/scripts.js"></script>
        <script src="https://code.jquery.com/jquery-3.4.1.slim.min.js" integrity="sha384-J6qa4849blE2+poT4WnyKhv5vZF5SrPo0iEjwBvKU7imGFAV0wwj1yYfoRSJoZ+n" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
        <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js" integrity="sha384-wfSDF2E50Y2D1uUdj0O3uMBJnjuUD4Ih7YwaYd1iqfktj0Uod8GCExl3Og8ifwB6" crossorigin="anonymous"></script>
        <script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.6.0/dist/js/bootstrap.bundle.min.js"></script>
    </body>
</html>
